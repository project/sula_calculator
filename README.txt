CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Themes
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The SULA Eligibility Calculator module creates a configurable block that
displays a simple AJAX based calculator.


REQUIREMENTS
------------

This module requires the following modules:

 * Block (Drupal core)

RECOMMENDED THEMES
-------------------

 * Bootstrap (https://www.drupal.org/project/bootstrap):
   When enabled, the classes inside of the calculator will automatically
   make it fully responsive.

INSTALLATION
------------

 * Install and uninstall as normal for a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer SULA Calculator

     Perform administration tasks for sula calculator module.

 * Customize the module settings in Administration » Configuration »
   SULA Calculator.

MAINTAINERS
-----------

Current maintainers:
 * Matthew Sherman - matthewsherman25@gmail.com
 * Isaiah Nixon - isaiahnixon@gmail.com
 * Scott Worthington - perlwiz@gmail.com

This project has been sponsored by:
 * Maricopa County Community Colleges
